# ECMAScript 6

## Introduction

* ECMAScript 6, also known as ECMAScript 2015, is the latest version of the ECMAScript standard.
* This guide does not cover all of the ES6 features, for example - Symbols, proxies, generators aren't present.

New ES6 features covered:

* [arrow functions](#markdown-header-arrow-functions)
* [classes](#markdown-header-classes)
* [enhanced object literals](#markdown-header-enhanced-object-literals)
* [template literals](#markdown-header-template-literals)
* [destructuring](#markdown-header-destructuring)
* [default parameters](#markdown-header-default-parameters)
* [rest operator](#markdown-header-rest-operator)
* [spread operator](#markdown-header-spread-operator)
* [let & const](#markdown-header-let--const)
* [for..of](#markdown-header-forof)
* [modules](#markdown-header-modules-importexport)
* [set & weakset](#markdown-header-set-weakset)
* [map & weakmap](#markdown-header-map-weakmap)
* [number, string, array & object apis](#markdown-header-number-string-array-object-apis)
* [promises](#markdown-header-promises)
* [async await](#markdown-header-asyncawait)
* [missing polyfills](#markdown-header-missing-polyfills)

## ECMAScript 6 Features

### Arrow Functions

* Shorthand for functions using the `=>` syntax.  
* Implicit return supported.
* Share same `this` (context) as surrounding code, so no need to use `.bind(this)`.
* Preferred syntax to use for anonymous functions.

‍
```JavaScript
// Implicit return
var odds = evens.map(v => v + 1);
var nums = evens.map((v, i) => v + i);
var pairs = evens.map(v => ({even: v, odd: v + 1})); // returning objects need to surround with `()` otherwise it will be evaluated as a block rather than an object

// Statement bodies
nums.forEach(v => {
  if (v % 5 === 0) {
    fives.push(v);
  }
});

// Lexical this
var bob = {
  _name: "Bob",
  _friends: [],
  printFriends() {
    this._friends.forEach(f => {
      console.log(this._name + " knows " + f);
    });
  }
};
```

More info: [MDN Arrow Functions](https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Functions/Arrow_functions)
     
----

### Getter/Setters

* Do not use because:
    * [Spread syntax](#spread-operator) doesn't trigger setters.
    * Class based getters/setters are prototype properties, so will not show up in any code which only shows objects own properties.
    * Easy to create silent errors, eg, if you misspell a setter/getter then you'll get undefined but if you misspelled a traditional getXProp/setXProp method you get an error.

----

### Classes

* ES6 classes are a simple sugar over the prototype-based OO pattern.  
* Classes support prototype-based inheritance, super calls, instance and static methods and constructors.
* Class field declarations are not currently enabled.
* Only have single super class, multiple inheritance not supported.
* Composition should be used/tried before reaching for Class hierarchies.
* Make sure to call `super` before accessing this on any derived class, otherwise you will get an error.

‍
```JavaScript
class Polygon {
  constructor(height, width) {
    this.name = "Polygon";
    this.height = height;
    this.width = width;
  }

  getHelloPhrase() {
    return `Hi, I am a ${this.name}`;
  }
}

class Square extends Polygon {
  constructor(length) {
    // Here, it calls the parent class' constructor with lengths
    // provided for the Polygon's width and height
    super(length, length);
    // Note: In derived classes, super() must be called before you
    // can use 'this'. Leaving this out will cause a reference error.
    this.name = "Square";
    this.length = length;
  }

  getCustomHelloPhrase() {
    const polygonPhrase = super.getHelloPhrase(); // accessing parent method with super.X() syntax
    return `${polygonPhrase} with a length of ${this.length}`;
  }

  methodUsingClassMethod() {
    // access static method by using class name
    const a = Polygon.someStaticMethod();
    
    // access static method by referring to the constructor
    const b = this.constructor.someStaticMethod();
    return b;
  }
  
  static someStaticMethod(){
    return "static method called";
  }
}

const mySquare = new Square(10);
console.log(mySquare.getHelloPhrase()); // "Hi, I am a Square" -- Square inherits from Polygon and has access to its methods
console.log(mySquare.getCustomHelloPhrase()); // "Hi, I am a Square with a length of 10"
console.log(Square.someStaticMethod()); //  "static method called"

```

More info: [MDN Classes](https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Classes)

----

### Enhanced Object Literals

Object literals are extended to support:
* Shorthand property assignments.
* Method declarations.
* Super calls.
* Computing property names with expressions.  

Together, these also bring object literals and class declarations closer together, and let object-based design benefit from some of the same conveniences.

‍
```JavaScript

const handler = () => "in handler";
const aPropName = "somePropNameHere";

const obj = {
    // Shorthand for 'handler: handler'
    handler,
	
    // Methods
    toString() {
     // Super calls
     return "d " + super.toString();
    },
	
    // Computed property names
    [ "prop_" + aPropName ]: 42
};

console.log(obj.handler()) => "in handler";
console.log(obj.toString()) => "d [object Object]";
console.log(obj.prop_somePropNameHere); // => 42

```

More info: [MDN Grammar and types: Object literals](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Grammar_and_types#Object_literals)

----

### Template Literals

* Provide string interpolation.
* Can span across multiple lines.
* Use backticks ` for syntax - **not** single quotes '.

‍
```JavaScript

//Pre ES6
const preEs6MultLineString = 'Pre ES6 \
you needed this';

// Multiline strings
const templateLiteralMagicString = `Now this is
 is magic.`;

// String interpolation
var name = "Bob";
var time = "today";
`Hello ${name}, how are you ${time}?`;

```

More info: [MDN Template Strings](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/template_strings)

----

### Destructuring

* Destructuring allows binding using pattern matching.
* Can be used with arrays and objects.  
* Destructuring is fail-soft, similar to standard object lookup `foo["bar"]`, producing `undefined` values when not found.
* Will error when destructing `null` or `undefined`.

‍*Note:* When refactoring object/class properties be careful of destructuring usages with ES6. Some IDEs will not automatically rename variables created via destructuring!

```JavaScript
// list matching
var [a, , b] = [1,2,3];


// object matching
const user = {
  name: "some guy",
  address: {
    city: "london"
  }
};

const {name, address: {city}} = user;
console.log(name); // 'some guy'
console.log(city); // 'london'

// Can be used in parameter position
function g({name: x}) {
  console.log(x);
}
g({name: 5});

// Fail-soft destructuring
function test({a}){
  return a
}

test(1); // undefined
test('fljdsal'); // undefined
test([300]); // undefined
test({b: 500}); // undefined
test({a: 10000}); // 10000

// will error when destructuring undefined/null
test(undefined); // Errors
test(null); // Errors

```

More info: [MDN Destructuring assignment](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Destructuring_assignment)

----

### Default Parameters

* Used in functions.
* Used in destructuring.
* Only uses default value when no parameter provided or parameters value is `undefined`.
* Will **not** replace `null` with the default value!

‍
```JavaScript

// Array destructuring with default
var [a = 1] = [];
a === 1;

function myFunc(x = 10) {
  return x;
}
console.log(myFunc()); // 10 -- no value is provided so x default value 10 is assigned to x in myFunc
console.log(myFunc(5)); // 5 -- a value is provided so x is equal to 5 in myFunc

console.log(myFunc(undefined)); // 10 -- undefined value is provided so default value is assigned to x
console.log(myFunc(null)); // null -- a value (null) is provided, see below for more details

```

More MDN info: [Default parameters](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Default_parameters)

----

### Rest Operator

* Bind trailing parameters to a real array.
* Rest replaces the need for `arguments`.

‍
```JavaScript

function f(x, ...y) {
  // y is an Array
  return x * y.length;
}
f(3, "hello", true) == 6;

// can also be used in destructuring
const a = [1, 2, 3, 4];
const [b, ...c] = a;
console.log(c) // => [2, 3, 4]

```

[Rest parameters](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/rest_parameters)

----

### Spread Operator

* Turn an array into consecutive arguments in a function call.  
* Useful when:
   * Shallow copying objects.
   * Concat arrays.
* Gotchas:
   * Will not copy prototype properties, only objects own props.
   * Does not trigger setters.
   
‍
```JavaScript

function f(x, y, z) {
  return x + y + z;
}

// Pass each elem of array as argument
f(...[1,2,3]) == 6;

// concat array
const arr1 = ["a", "b", "c"];
const arr2 = [...arr1, "d", "e", "f"]; // ["a", "b", "c", "d", "e", "f"]

// shallow copy objects
const obj1 = { x: 1, y: 2};
const obj2 = { x: 100};
const obj3 = {...obj1, ...obj2, z: 100} ;
obj3 // { x: 100, y: 2, z: 100}

```

[Spread Operator](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Spread_operator)

----

### Let & Const

* Unlike `var`, `let` and `const` statements are not initialised to `undefined` when hoisted to the top of their enclosing scope, temporal deadzone.
* They are block scoped.
   * *Be cautious with switch statements.  All cases count as the same block level as the switch itself*. Declaring the same named variable with `let` or `const` in different cases will result in an error.
* `const` is single-assignment.
* Careful replacing if `var` for `let` or `const` when `var` wasn't declared at top of a function because of variable hoisting.
* Aim to primarily use `const` for all variables whose values never change.

‍
```JavaScript

// temporal deadzone
function do_something() {
  console.log(bar); // undefined
  console.log(foo); // ReferenceError
  var bar = 1;
  let foo = 2;
}


// old var version
var snack = "Meow Mix";
function getFood(food) {
    if (food) {
		// due to "var"  snack is hoisted to top of function with value of undefined
        var snack = "Friskies";
        return snack;
    }
    return snack;
}
getFood(false); // undefined

// replacing var with let example
let snack = "Meow Mix";
function getFood(food) {
    if (food) {
        // in if block, snack is a new variable
        // only exists inside the if block
        let snack = 'Friskies';
        return snack;
    }
    return snack;
}
getFood(false); // 'Meow Mix'


const a = 1;
a = 2; // error

// 

```

More MDN info: [let statement](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/let), [const statement](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/const)

----

### For..Of

* Iterates over Iterable objects including: built-in String, Array, Array-like objects (e.g., arguments or NodeList), Map, Set.
* **Plain objects are not iterable**.
* Main difference to for..in is.
   * for..in - iterates over enumerable props.
   * for..of - iterates over values the Iterable Object defines to be iterable. 

A object created with it's own iterator property means it can customise how iteration behaves when used in combination with for..of.

‍
```JavaScript

let articleParagraphs = document.querySelectorAll('article > p');

for (let paragraph of articleParagraphs) {
  paragraph.classList.add('read');
}
```

More info: [MDN for...of](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for...of)

----

### Modules (import/export)

* Avoid `import * as everything from "./myModule"` use explicit imports.
* Typo in names wont error, will be `undefined`.
* "dynamic" importing is possible (used for lazying loading), but only with hardcoded strings. An example can be found in Plus's routing.
* Rule of thumb - 1 file, 1 purpose, 1 export.

‍Recommended usages:
```JavaScript
//myClassModule.js
export class MyClass {}

//myFunctionModule.js
export function myFunc(obj) {
  return {...obj};
};

//myNamedModule.js
function myOtherFunc() {}
export { myOtherFunc as renamedMyFunc }; // named with alias

//in another file
import { MyClass } from "./myClassModule";
import { myFunc } from "./myFunctionModule";
import { renamedMyFunc } from "./myNamedModule";
```
    
‍
Other syntax available:
```JavaScript

// in myModule.js
export const a = 1;
export function myFunc(obj) {
  return {...obj};
};
export const someObj = {aProp: 'here'};
export class MyClass {
  /* your class code here*/
}
export { myFunc as renamedMyFunc }; // named with alias
export default myFunc; // default export


// in another file
import theDefaultExport1 from "./myModule";
import { exportDoesntExist, export2 } from "./myModule";
import { renamedMyFunc } from "./myModule";
import { someObj as someAlias } from "./myModule";


// avoid using "*" imports as they'll pull in everything from the lib
import * as everything from "./myModule"; 
import theDefaultExport2, * as everything2 from "./myModule";


console.log('incorrect name will just be undefined', exportDoesntExist) //  => undefined

// dynamically import a module
async function lazyLoad(){
   module = await import("../path/to/some/file"); // can't use vars in dynamic imports, eg, `import(myVarContainingPath).then(...`
   return moduleCode.aModuleMethod();
}


```
    
‍
Brief summary of module types:

* **Named / explicit**

    Export / import specific named items. Great for making things modular and is the crux of effective [tree shaking](https://developer.mozilla.org/en-US/docs/Glossary/Tree_shaking).
  
```javascript
     //EXPORT
     export thingOne;
     export { thingTwo, thingThree }; //Don't recommend!

     //-----------------------------------------
 
     //IMPORT
     import { thingOne, thingTwo, thingThree } from "./lotsOfThings";
```
    
* **Default** 

    Commonly used amongst older libraries, default exposes one value which can be imported using any name without the need for the `as` keyword.
   
```javascript
     //EXPORT
     export default myThing;

     //-----------------------------------------
     
     //IMPORT
     import myThing from "./myThing";
     //OR
     import renamedMyThing from "./myThing";  //renamedMyThing === myThing
```
   
* **All `*`**

    Groups all exports under a single accessor.
   
```javascript
     //EXPORT
     export thingOne;
     export thingTwo;
     export thingThree;
     
     //-----------------------------------------
     
     //IMPORT 
     import * as things from "./lotsOfThings";
     //things.thingOne;
     //things.thingTwo;
     //things.thingThree;
```
 
 * **Side-effects**

    Import code which registers side-effects. Commonly used for older libraries register global window items or things which self-register themselves (e.g. AngularAMD AngularJs components, service, .etc). 
    
```javascript
     //EXPORT
     window.CKEDITOR = CKEDITOR;
     
     
     //-----------------------------------------

     //IMPORT
     import "./ckEditor";
     //window.CKEDITOR is now available.
```


More MDN info: [import statement](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/import), [export statement](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/export)!

    
‍
----

### Set & WeakSet

* List of unique values, useful when you don't want duplicates.
* Uses === for equality so object clones won't be deduplicated.
* WeakSets:
   * Contain objects only, not primatives.
   * Only allow objects to be present which are referred to elsewhere.
   * Are **not** enumerable.
   
‍
```JavaScript

const s = new Set();

const obj1 = {id: 1};
const obj2 = {...obj1};

s.add("hello").add("goodbye").add("hello");

console.log(s.size); // => 2

s.add(obj1).add(obj1);

console.log(s.size); // => 3

// uses === to determine equality, so obj clones won't be deduplicated
s.add(obj2);
console.log(s.size); // => 4


// Weak Sets are for objects only
const ws = new WeakSet();
ws.add(42) => throws Error;

// the added object below has no other references, so it will not be held in the set
ws.add({ data: 42 });
```

More MDN info: [Set](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Set), [WeakSet](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/WeakSet)

----

### Map & WeakMap

* Map is an iterable key value store.
* Unlike Object hash maps it can store any type as a key and it will not be converted to a string.
* WeakMap:
    * Provides leak-free object-key'd side tables.
    
‍
```JavaScript
let map = new Map();
map.set('name', 'someguy');
map.get('name'); // someguy
map.has('name'); // true

// maps can use any type as key
let map = new Map([
    ['name', 'someguy'],
    [true, 'false'],
    [1, 'one'],
    [{}, 'object'],
    [function () {}, 'function']
]);
for (let key of map.keys()) {
    console.log(typeof key);
    // > string, boolean, number, object, function
}


// Weak Maps

let map = new WeakMap();
let el  = document.getElementById('someElement');

// Store a weak reference to the element with a key
map.set(el, 'reference');

// Access the value of the element
let value = map.get(el); // 'reference'

// Remove the reference
el.parentNode.removeChild(el);
el = null;

// map will be empty, since the element is destroyed

```

More MDN info: [Map](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map), [WeakMap](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/WeakMap)

----

### Number, String, Array & Object APIs

Many new library additions, including core Math libraries, Array conversion helpers, String helpers .etc.

‍
```JavaScript
Number.isInteger(Infinity); // false
Number.isNaN("NaN"); // false

"abcde".includes("cd"); // true
"abc".repeat(3); // "abcabcabc"

Array.from(document.querySelectorAll('*')); // Returns a real Array
Array.of(1, 2, 3); // [1, 2, 3] Similar to new Array(...), but without special one-arg behavior
[0, 0, 0].fill(7, 1); // [0,7,7]
[1, 2, 3].findIndex(x => x === 2); // 1
[1, 2, 3].findIndex(x => x === 4); // -1
[1, 2, 3].find(x => x === 3); // 3
[1, 2, 3].find(x => x === 4); // undefined

```

More MDN info: [Number](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number), [Math](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math), [Array.from](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/from), [Array.of](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/of)

----

### Promises

* Similar to AngularJs promises - including support for finally via polyfill.
* Does not automatically trigger digest cycle like AngularJs promises.

‍
```JavaScript

function timeout(duration = 0) {
    return new Promise((resolve, reject) => {
        setTimeout(resolve, duration);
    })
}

const p = timeout(1000).then(() => {
    return timeout(2000);
}).then(() => {
    throw new Error("hmm");
}).catch(err => {
    return Promise.all([timeout(100), timeout(200)]);
});

```

More info: [MDN Promise](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise)

----

### Async/Await

* Makes it possible to use promises as if they were synchronous.
* Can be used with AngularJs promises.  

‍
```JavaScript

var request = require('request');

function getJSON(url) {
  return new Promise(function(resolve, reject) {
    request(url, function(error, response, body) {
      resolve(body);
    });
  });
}

async function main() {
  try{
    const data = await getJSON();
    console.log(data); // contains data from request
  } catch(e) {
    // will be called if getJSON promise rejects 
	// OR if any sync code has js exceptions
  }
}

main();

```

----

### Missing Polyfills

* fetch
* proxy
* Intl
* [See missing poylfills for more info!](https://github.com/zloirock/core-js/tree/v2#missing-polyfills)

